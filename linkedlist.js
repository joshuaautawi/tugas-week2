

class Node{
    constructor(data){
        this.data = data
        this.next = null
    }
}

class LinkedList{
    constructor(){
        this.head = null
        this.tail = null
        this.size = 0
    }

    listPrint(){
        let data = ""
        let current = this.head
        while(current != null ){
            data = data + current.data + " "
            current = current.next
        }
        return data
    }

    isNotEmpty(){return this.size > 0 ? true : false}

    linkSize(){return this.size}

    addFront(data){
        let newNode = new Node(data)
        if(this.size <= 0 ){
            this.head = newNode
            this.tail = newNode
        }
        else{
            newNode.next = this.head
            this.head = newNode
        }
        this.size++
    }

        listAdd(data){
            let newNode = new Node(data)
            if(this.size == 0){
                this.head = newNode
                this.tail = newNode
            }
            else{
                const temp = this.tail
                this.tail = newNode
                temp.next = this.tail
            }
            this.size++   
        }
    

    insertAt(data,index){
        const newNode = new Node(data)
        if(index< 0 || index> this.size){return null}
        if(index == 0){return this.addFront(data)}
        else if(index == this.size){return this.listAdd(data)}
        else{
            let counter = 0
            let prev = null
            let current = this.head
            while(counter<index){
                prev = current
                current = current.next
                counter++
            }
            prev.next = newNode
            newNode.next = current
            
            this.size ++
        }
    }

    removeLast(){
        if(this.size === 0 ){
            return null
        }
        let data =this.tail.data
        if(this.size ===1 ){
            this.head =null
            this.tail = null
        }
        else{
            let current = this.head
            while(current.next.next != null ){
                current = current.next
            }
            current.next = null
            this.tail = current
        }
        this.size --
        return data
    }
    removeFrom(index){
        if(index < 0|| index >= this.size){
            return null
        }
        if(index == 0){
            return this.removeFirst()
        }
        if(index == this.size-1){
            return this.removeLast()
        }
        else{
            let current = this.head
            let counter = 0
            let prev = null
            while(counter<index){
                prev = current
                current = current.next
                counter++
            }
            prev.next = current.next
            this.size -- 
            return current.data
        }
    }

    indexOf(data){
        if(this.size <= 0){
            return null
        }
       else{
            let current = this.head
            let counter = 0
            while(counter < this.size){
                if(current.data == data){
                    return counter
                }
                else if(current.data != data ){
                    current = current.next
                    counter++
                }
            }
            return null
       }
    }
    removeElement(value){
        let counter = 0
        let prev= null
        let current = this.head
    
        if(this.size == 0){
            return null
        }
        
        if(this.size == 1){
            if(current.data ==value){
                this.head = null
                this.tail = null
                return value
            }
        
        }
        else{
            while(counter < this.size){
                if(current.data != value){
                    prev = current
                    current = current.next
                
                }
                else if(current.data == value){
                   
                    prev.next = current.next
                    current = prev
                    this.size --
                    return value
                }
                counter++
            }
            return null
        }}
}

var ll = new LinkedList()
console.log(ll.isNotEmpty())
ll.listAdd("a")
console.log(ll.listPrint())
console.log(ll.linkSize())
ll.listAdd("b")
ll.listAdd("c")
ll.listAdd("d")
ll.listAdd("e")
console.log(ll.listPrint())
console.log("remove by search element : " +ll.removeElement("d"))
console.log(ll.listPrint())
console.log("Index of 'c' : " + ll.indexOf("c"))
ll.insertAt("z",2)
console.log(ll.listPrint())
console.log("is List Not Empty : "+ll.isNotEmpty())
console.log("remove by indexed elemet : " + ll.removeFrom(3))
console.log(ll.listPrint())

